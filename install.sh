#!/usr/bin/env bash

source ./.env

echo "*** Installing wordpress... ***"

docker-compose run --rm --no-deps wpcli core install --skip-email --url=wp.${APP_URL} --title="WeCode WP Boilerplate" --admin_user="${WP_USER_LOGIN}" --admin_email="admin@wecode.agency" --admin_password="${WP_USER_PASSWORD}"

echo "*** Wordpress is ready! ***"

echo "*** Installing plugins... ***"

docker-compose run --rm --no-deps wpcli plugin install timber-library
docker-compose run --rm --no-deps wpcli plugin activate timber-library

docker-compose run --rm --no-deps wpcli plugin install admin-menu-tree-page-view
docker-compose run --rm --no-deps wpcli plugin activate admin-menu-tree-page-view

docker-compose run --rm --no-deps wpcli plugin install contact-form-7
docker-compose run --rm --no-deps wpcli plugin activate contact-form-7

docker-compose run --rm --no-deps wpcli plugin install wp-smushit
docker-compose run --rm --no-deps wpcli plugin activate wp-smushit

docker-compose run --rm --no-deps wpcli plugin install wordpress-seo
docker-compose run --rm --no-deps wpcli plugin activate wordpress-seo

docker-compose run --rm --no-deps wpcli plugin install svg-support
docker-compose run --rm --no-deps wpcli plugin activate svg-support

docker-compose run --rm --no-deps wpcli plugin install wp-media-folder
docker-compose run --rm --no-deps wpcli plugin activate wp-media-folder

docker-compose run --rm --no-deps wpcli plugin install wordpress-seo
docker-compose run --rm --no-deps wpcli plugin activate wordpress-seo

#docker-compose run --rm --no-deps wpcli plugin install timber-library
#docker-compose run --rm --no-deps wpcli plugin activate timber-library

docker-compose run --rm --no-deps wpcli plugin delete hello

echo "*** Plugins are ready! ***"