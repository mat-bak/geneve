## WeCode WP

###  WP Version

New project instruction:

1. Clone repo ```git clone git@gitlab.wecode.agency:wecode/wp-bolierplate.git .```.

2. Copy ```cp .env.default .env``` and set ```.env``` variables. $APP_BRAND must be unique slug.

3. Run docker: ```docker-compose up -d```

4. Install Wordpress and Plugins: ```./install.sh```

5. Generate and activate theme: ```./theme.sh```.

6. Add url to host in .hosts file: ```
                         127.0.0.1	wp.BRAND.wecode.local
                         ```

7. Visit your domain: ```http://wp.BRAND.wecode.local```


