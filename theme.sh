#!/usr/bin/env bash

source ./.env

clear

echo -e "\e[38;5;202m~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\e[0m"
echo -e "\e[38;5;202m    *** Run theme generator ***\e[0m"
echo -e "\e[38;5;202m~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\e[0m"

echo "Creating '${APP_BRAND}' theme..."

cat > www/project/wp-content/themes/wecodewp/style.css <<EOL
/*
Theme Name: ${APP_BRAND}
Theme URI: https://wecode.agency/
Author: the WeCode team
Author URI: https://wecode.agency/
Description: WeCodeWp for ${APP_BRAND}.
Version: 1.0
License: All rights reserved
Text Domain: wecodewp
*/
EOL

echo "Created!"

mv www/project/wp-content/themes/wecodewp www/project/wp-content/themes/${APP_BRAND}
docker-compose run --rm --no-deps wpcli theme activate ${APP_BRAND}

echo -e "\e[32mActivated!\e[0m"

echo "Instaling dependencies..."

docker run --rm -it --volume $PWD/www/project/wp-content/themes/${BRAND}/:/app composer install --ignore-platform-reqs

cd www/project/wp-content/themes/${APP_BRAND}

npm install
composer install
npm run build

echo -e "\e[32m~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\e[0m"
echo -e "\e[32m   Theme is ready! Have fun!\e[0m"
echo -e "\e[32m~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\e[0m"
