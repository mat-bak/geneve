import $ from 'jquery';

class HeaderPrimaryModule {
    constructor($header) {
        this.$window = $(window);
        this.$header = $header;
        this.$trigger = $header.find('.m-Hamburger');
        this.$navigation = $header.find('.m-NavigationPrimary');

        this.state = {
            open: false,
        };
    }

    hideNavigation() {
        this.$trigger.removeClass('m-Hamburger--active');
        this.$header.removeClass('m-HeaderPrimary--open');
        this.state.open = false;
    }

    showNavigation() {
        this.$trigger.addClass('m-Hamburger--active');
        this.$header.addClass('m-HeaderPrimary--open');
        this.state.open = true;
    }

    events() {
        this.$window.on('scroll', () => {
            if (100 > window.pageYOffset) {
                this.$header
                    .addClass(this.$header.data('theme-modificator'))
                    .removeClass('m-HeaderPrimary--collapse');
            } else {
                this.$header
                    .removeClass(this.$header.data('theme-modificator'))
                    .addClass('m-HeaderPrimary--collapse');
            }
        });
        this.$trigger.on('click', e => {
            e.preventDefault();
            if (this.state.open) this.hideNavigation();
            else this.showNavigation();
        });

        $(document).on('click', e => {
            if (this.state.open) {
                if (
                    !this.$header.is(e.target) &&
                    0 === this.$header.has(e.target).length
                ) {
                    this.hideNavigation();
                }
            }
        });
    }

    init() {
        this.events();
    }
}

export default HeaderPrimaryModule;
