<?php

/*
 * Key of the array with specific block settings should be Upper Camel Case and the same as folder name in library/blocks
 * Sample OK:
 * 'blocks' => [
 * 		'BlockName' => []
 * ];
 * Sample Error:
 * 'blocks' => [
 * 		'Block Name' => []
 * ];
 */
$config['gutenberg_blocks'] = [
	'path' => get_stylesheet_directory() . '/src/library/blocks',
	'blocks' => [
		'Testimonials' => [
			'name'              => 'Testimonials',
			'title'             => 'Testimonials',
			'category'          => 'wecode',
			'icon'              => 'tide',
		],

	]
];
