<?php

$config['embed_css'] = [
	'main-styles' => '/static/css/app.bundle.css'
];

$config['embed_admin_css'] = [
	'admin-main-styles' => '/static/css/admin.bundle.css'
];
