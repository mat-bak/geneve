<?php

namespace WCD;

/**
 * Class GutenbergBlocks
 * @package WCD
 */
class GutenbergBlocks
{
	/**
	 * @var mixed
	 */
	private $_blocks_enabled;

	/**
	 * @var mixed
	 */
	private $_blocks_path;

	/**
	 * @var mixed
	 */
	private $_blocks_slugs;

	/**
	 * GutenbergBlocks constructor.
	 * @param $blocksConfig
	 */
	public function __construct($blocksConfig)
	{
		// read block
		$this->_blocks_enabled = $blocksConfig['blocks'];
		$this->_blocks_path = $blocksConfig['path'];
		$this->_blocks_slugs = [];

		// register gutenberg blocks
		add_action('acf/init', [$this, 'register_blocks']);
	}

	/**
	 * Reads blocks defined in config and fires their registration in WP
	 */
	public function register_blocks()
	{
		if (function_exists('acf_register_block'))
		{
			foreach ($this->_blocks_enabled as $blockSlug => $blockData)
			{
				// load block config file if exists, otherwise use default rendering method
				if (file_exists($this->_blocks_path . '/' . $blockSlug . '/' . $blockSlug . '.block.render.php'))
					require_once $this->_blocks_path . '/' . $blockSlug . '/' . $blockSlug . '.block.render.php';

				if (function_exists('acf_block_render_callback_' . $blockSlug))
					$blockCallback = 'acf_block_render_callback_' . $blockSlug;
				else
					$blockCallback = [$this, 'render_block'];

				$this->_blocks_slugs[strtolower($blockSlug)] = $blockSlug;
				$this->_register_block($blockSlug, $blockData, $blockCallback);


				add_action('acf/update_field_group', [$this, 'set_up_save_path'], 1, 1);
			}
		}
	}

	/**
	 * Renders default block template file
	 * @param $block
	 */
	public function render_block($block)
	{
		$name = str_replace('acf/', '', $block['name']);
		$slug = $this->_blocks_slugs[$name];

		$context = \Timber::get_context();

		$context['block'] = $block;
		$context['fields'] = get_fields();

		\Timber::render($this->_blocks_path . '/' . $slug . '/' . $slug . '.block.view.twig', $context);
	}

	/**
	 * @param $group
	 */
	public function set_up_save_path($group)
	{

		// make sure that this group is for one block and only that block (no other post type, or many blocks)
		if (is_array($group['location']) && count($group['location']) == 1
			&& is_array($group['location'][0]) && count($group['location'][0]) == 1)
		{
			foreach ($group['location'] as $location)
				foreach ($location as $detail)
					if ($detail['param'] == 'block')
					{
						// now we know it's just one condition and it's block
						$blockSlugName = $detail['value'];


						// try to match condition block slug with blocks defined in config
						foreach ($this->_blocks_enabled as $blockSlug => $block)
						{
							if ($blockSlugName == 'acf/' . sanitize_title($block['name'])) ;
							{
								// found matching block - make JSON folder within this block in library and save config in there
								if (is_dir($this->_blocks_path . '/' . $blockSlug))
								{
									global $jsonPath;
									$jsonPath = $this->_blocks_path . '/' . $blockSlug . '/json';
									if (!is_dir($jsonPath))
										mkdir($jsonPath);

									add_action('acf/settings/save_json', function () {
										global $jsonPath;
										return $jsonPath;
									}, 999);
								}

							}

						}

					}
		}
		return $group;
	}

	/**
	 * Registers block within ACF Pro
	 * @param string $blocksSlug
	 * @param array $blockData
	 * @param $blockCallback
	 */
	private function _register_block(string $blocksSlug, array $blockData, $blockCallback)
	{
		if ($blockData['name'])
			$blockName = $blockData['name'];
		else
			$blockName = str_replace('_', ' ', ucfirst($blocksSlug));

		acf_register_block(array(
			'name' => $blocksSlug,
			'title' => $blockName,
			'description' => isset($blockData['description']) ? $blockData['description'] : sprintf(__('%s description.'), $blockName),
			'render_callback' => $blockCallback,
			'category' => isset($blockData['category']) ? $blockData['category'] : 'formatting',
			'icon' => isset($blockData['icon']) ? $blockData['icon'] : 'admin-comments',
			'keywords' => isset($blockData['keywords']) ? $blockData['keywords'] : [],
			'mode' => isset($blockData['mode']) ? $blockData['mode'] : 'edit',
		));

		if (is_dir($this->_blocks_path . '/' . $blocksSlug . '/json'))
		{
			$acfService = new Acf\Service();
			$acfService->register_json_load_dir($this->_blocks_path . '/' . $blocksSlug . '/json');
		}
	}


}
